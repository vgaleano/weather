<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\UrlHelper;

class WeatherController extends AbstractController
{
    private $urlHelper;

    public function __construct(UrlHelper $urlHelper)
    {
        $this->urlHelper = $urlHelper;
    }

    public function index()
    {
        return $this->redirectToRoute('city', [
            'city' => 'Miami'
        ]);
    }

    public function city($city)
    {
        $list_location = ["Miami", "Orlando", "New York"];
        if (!in_array($city, $list_location)) {
            return $this->render('weather/error.html.twig', [
                'titulo' => 'NOT FOUND'
            ]);
        }

        $principal = [
            'temp_c' => 'NA',
            'text' => 'NA',
            'humidity' => 'NA'
        ];
        $name = $city;

        //Consulta al api
        $request = $this->getWeatherForecastByLocation(str_replace('%20',' ',$city));
        $content = $request->getContent();
        $content = json_decode($content, true);
        if ($content['success']==true) {
            $info = $content['data'];
            $name = $info['location']['name'].", ".$info['location']['region'].",<br> ".$info['location']['country'];
            $principal = [
                'temp_c' => $info['current']['temp_c'].'°c',
                'text' => $info['current']['condition']['text'],
                'humidity' => $info['current']['humidity'].'%'
            ];
            
            $path = $this->urlHelper->getAbsoluteUrl('/weather/weather_conditions.json');
            $weather_conditions = file_get_contents($path);
            $jsonData = json_decode($weather_conditions,1);
            $code_condition = $info['current']['condition']['code'];
            $condition=array_filter($jsonData, function ($tT) use ($code_condition) 
            {
                if ($code_condition==$tT['code']) {
                    return $tT;
                }
            });
            $condition=array_values($condition);
            if (count($condition)>0) {
                $path_condition = "weather/".($info['current']['is_day']==1?'day':'night')."/{$condition[0]['icon']}.png";
            }

            $current_detail = [
                [
                    'label' => 'Fecha',
                    'value' => $info['current']['last_updated'],
                    'icon' => 'clock'
                ],
                [
                    'label' => 'Humedad',
                    'value' => $info['current']['humidity'].'%',
                    'icon' => 'tint'
                ],
                [
                    'label' => 'Humedad',
                    'value' => $info['current']['pressure_mb'].' mb',
                    'icon' => 'compress'
                ],
                [
                    'label' => 'Nubosidad',
                    'value' => $info['current']['cloud'].'%',
                    'icon' => 'cloud'
                ],
                [
                    'label' => 'Viento',
                    'value' => $info['current']['vis_km'].' km/h',
                    'icon' => 'recycle'
                ],
                [
                    'label' => 'Ráfagas de viento',
                    'value' => $info['current']['gust_kph'].' km/h',
                    'icon' => 'exclamation-triangle'
                ]
            ];

            $pronostico_detail = $info['forecast']['forecastday'][0]['hour'];

            $pronostico_days = [];
            for ($i=0; $i < count($info['forecast']['forecastday']); $i++) { 
                $pronostico_days[] = [
                    'date' => $info['forecast']['forecastday'][$i]['date'],
                    'condition' => $info['forecast']['forecastday'][$i]['day']['condition'],
                    'humidity' => $info['forecast']['forecastday'][$i]['day']['avghumidity']
                ];
            }
        }

        return $this->render('weather/index.html.twig', [
            'location' => $city,
            'full_name' => $name,
            'all_name' => \str_replace('<br>', ' ', $name),
            'titulo' => \str_replace('<br>', ' ', $name).' | Weather',
            'localTime' => date('Y-m-d'),
            'latitud' => isset($info) ? $info['location']['lat'] : '',
            'longitud' => isset($info) ? $info['location']['lon'] : '',
            'code_condition' => isset($path_condition) ? $path_condition : '',
            'current_detail' => isset($current_detail) ? $current_detail : [],
            'principal' => isset($principal) ? $principal : [],
            'pronostico_detail' => isset($pronostico_detail) ? $pronostico_detail : [],
            'jsonData' => isset($jsonData) ? $jsonData : [],
            'pronostico_days' => isset($pronostico_days) ? $pronostico_days : []
        ]);
    }

    //SERVICIOS

    public function getWeatherForecastByLocation($city) {
        try {
            $url = "http://api.weatherapi.com/v1/forecast.json?";        
            $data = array(
                'key=78b70e74002044ae84c224029211508',
                "q=$city",
                "days=7",
                'alerts=no',
                'aqi=no',
                'lang=es'
            );
            $data = implode('&', $data);
            $ch = curl_init($url.$data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            $result = json_decode($result, true);
            if (is_null($result) || array_key_exists('error', $result)) {
                return new JsonResponse([
                    'success' => false,
                    'code'    => $result['error']['code'],
                    'message' => $result['error']['message'],
                ]);
            }else{
                return new JsonResponse([
                    'success' => true,
                    'data' => $result
                ]); 
            }
        } catch (\Exception $exception) {
            return new JsonResponse([
                'success' => false,
                'code'    => $exception->getCode(),
                'message' => $exception->getMessage(),
            ]);
        }
        
        return new Response($city);
    }

    private function objectToArray ( $object ) {
        if(!is_object($object) && !is_array($object)) {
          return $object;
        }  
        return array_map( 'objectToArray', (array) $object );
    }

}

