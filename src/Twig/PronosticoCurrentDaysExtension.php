<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class PronosticoCurrentDaysExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('filter_name', [$this, 'doSomething']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('pronosticoDays', [$this, 'doSomething']),
        ];
    }

    public function doSomething($dias, $jsonData)
    {
        date_default_timezone_set('America/Bogota');
        $content = "";

        for ($i=0; $i < count($dias); $i++) { 
            $code_condition = $dias[$i]['condition']['code'];
            $condition=array_filter($jsonData, function ($tT) use ($code_condition) 
            {
                if ($code_condition==$tT['code']) {
                    return $tT;
                }
            });
            $condition=array_values($condition);
            $path_condition = '';
            if (count($condition)>0) {
                $path_condition = "weather/day/{$condition[0]['icon']}.png";
            }
            $content .= '<div class="col">';
                $content .= '<div class="card weather-day text-center">';
                    $content .= '<div class="card-header">';
                        $content .= '<h5 class="mb-0">'.date('d', strtotime($dias[$i]['date'])).'</h5>';
                    $content .= '</div>';
                    $content .= '<div class="card-body py-0">';
                        $content .= "<img width='35px' src='../$path_condition'>";
                    $content .= '</div>';
                    $content .= '<div class="card-footer d-flex justify-content-center">';
                        $content .= '<h6><b>'.$dias[$i]['humidity'].'%</b></h6>';
                    $content .= '</div>';
                $content .= '</div>';
            $content .= '</div>';
        }
        
        return $content;
    }
}
