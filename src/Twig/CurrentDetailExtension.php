<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class CurrentDetailExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('filter_name', [$this, 'doSomething']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('currentDetail', [$this, 'doSomething']),
        ];
    }

    public function doSomething($info)
    {
        $content = "";
        for ($i=0; $i < count($info); $i++) { 
            $content .= '<div class="col my-1">';
                $content .= '<div class="box-info">';
                    $content .= '<div>';
                        $content .= '<i class="fas fa-'.$info[$i]['icon'].' fa-2x"></i>';
                    $content .= '</div>';
                    $content .= '<div>';
                        $content .= '<div class="text-left">';
                            $content .= '<h6>'.$info[$i]['label'].'</h6>';
                            $content .= '<small>'.$info[$i]['value'].'</small>';
                        $content .= '</div>';
                    $content .= '</div>';
                $content .= '</div>';
            $content .= '</div>';
        }
        return $content;
    }
}
