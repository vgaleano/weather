<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class CustomFiltersExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('navbar_active', [$this, 'navbarActive']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('navbarActive', [$this, 'navbarActive']),
        ];
    }

    public function navbarActive($city, $navbar)
    {
        return $city==$navbar ? 'active' : '';
    }
}
