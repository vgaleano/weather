<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class PronosticoCurrentExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('filter_name', [$this, 'doSomething']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('pronosticoCurrent', [$this, 'doSomething']),
        ];
    }

    public function doSomething($horas, $jsonData)
    {
        date_default_timezone_set('America/Bogota');
        $content = "";
        $current = date('Y-m-d H:00');

        for ($i=0; $i < count($horas); $i++) { 
            if ($horas[$i]['time'] >= $current) {
                $code_condition = $horas[$i]['condition']['code'];
                $condition=array_filter($jsonData, function ($tT) use ($code_condition) 
                {
                    if ($code_condition==$tT['code']) {
                        return $tT;
                    }
                });
                $condition=array_values($condition);
                $path_condition = '';
                $is_day = '';
                if (count($condition)>0) {
                    $is_day = $horas[$i]['is_day']==1?'day':'night';
                    $path_condition = "weather/$is_day/{$condition[0]['icon']}.png";
                }
                $content .= '<div>';
                    $content .= '<div class="card weather-day text-center '.$is_day.'">';
                        $content .= '<div class="card-header">';
                            $content .= '<h4>'.date('H:00', strtotime($horas[$i]['time'])).'</h4>';
                        $content .= '</div>';
                        $content .= '<div class="card-body">';
                            $content .= '<h6><small>'.$horas[$i]['condition']['text'].'</small></h6>';
                            $content .= "<img src='../$path_condition'>";
                        $content .= '</div>';
                        $content .= '<div class="card-footer d-flex justify-content-center">';
                            $content .= '<h6><b>'.$horas[$i]['humidity'].'%</b> - '.$horas[$i]['temp_c'].'°c</h6>';
                        $content .= '</div>';
                    $content .= '</div>';
                $content .= '</div>';
            }
        }
        
        return $content;
    }
}
